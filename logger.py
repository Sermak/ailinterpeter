import logging

log = logging.getLogger()
log.setLevel(level=logging.INFO)
FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)


def info(msg):
    log.info(msg)


def fatal(msg):
    log.fatal(msg)
