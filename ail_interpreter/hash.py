import hashlib


def fhash(x):
    return hashlib.sha3_224(x.encode()).hexdigest()
