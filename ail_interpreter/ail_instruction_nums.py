from line import line
from logger import fatal
import sys


def ail_to_int(ail, eip):
    if ail == "NOOP" or ail == "":
        return 0
    elif ail == "LABEL":
        return 1
    elif ail == "JUMP":
        return 2
    elif ail == "PRINT":
        return 3
    elif ail == "PRINT_VAR":
        return 4
    elif ail == "PUT":
        return 5
    elif ail == "MOVE":
        return 6
    elif ail == "ADD":
        return 7
    elif ail == "MULT":
        return 8
    elif ail == "CALL":
        return 9
    elif ail == "FUNCTION":
        return 10
    elif ail == "RETURN":
        return 11
    elif ail == "POP":
        return 12
    elif ail == "PUSH":
        return 13
    elif ail == "PUSH_VAR":
        return 14
    elif ail == "RET_POP":
        return 15
    elif ail == "RET_PUSH":
        return 16
    elif ail == "RET_PUSH_VAR":
        return 17
    elif ail == "COMPARE":
        return 18
    elif ail == "IF":
        return 19
    elif ail == "SUB":
        return 20
    elif ail == "DIV":
        return 21
    elif ail == "MOD":
        return 22
    elif ail == "EXP":
        return 23
    elif ail == "LOG":
        return 24
    elif ail == "TAU":
        return 25
    elif ail == "EULER":
        return 26
    elif ail == "UPSCOPE":
        return 27
    elif ail == "PROC_TIME":
        return 28
    else:
        fatal(line(eip) + ": Instruction code " + ail + " invalid")
        sys.exit(-1)
