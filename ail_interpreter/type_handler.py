def simplify_num(x):
    x = str(x)
    if x.endswith(".0"):
        return x[:-2]
    return x
