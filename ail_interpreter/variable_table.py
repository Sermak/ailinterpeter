from line import line
from logger import info, fatal
import sys


class VariableTable:
    table = {}

    def save_var(self, name, value, scope, eip):
        info(line(eip) + " - Saving var " + name + " in " + scope + " as " + value)
        self.table[(name, scope)] = value

    def retrieve_var(self, name, scope, eip):
        try:
            return self.table[(name, scope)]
        except KeyError:
            if scope == "/":
                fatal("VariableNotFoundException (Line " + line(eip) + ": Var " + name + " not found)")
                sys.exit(-1)
            else:  # fallback
                s = scope
                s = s[:s.rindex("/")]
                s = s[:s.rindex("/")] + "/"
                info(line(eip) + " - var " + name + " not in scope " + scope + " falling back on parent scope")
                return self.retrieve_var(name, s, eip)

    def collapse_scope(self, scope, eip):
        info(line(eip) + " - Collapsing scope " + scope)
        new_table = dict(self.table)
        for (name, sscope) in self.table.keys():
            if scope == sscope:
                del new_table[(name, sscope)]
        self.table = new_table

    def upscope_var(self, name, scope, eip):
        if scope == "/":
            return
        s = scope
        s = s[:s.rindex("/")]
        s = s[:s.rindex("/")] + "/"
        value_of_var = self.retrieve_var(name, scope, eip)
        info(line(eip) + " - Upscoping " + name + " in " + scope + " as " + value_of_var + " to scope " + s)
        del self.table[(name, scope)]
        self.save_var(name, value_of_var, s, eip)
