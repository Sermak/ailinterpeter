#ifndef LABEL_TABLE_H_INCLUDED
#define LABEL_TABLE_H_INCLUDED

// initialize the label table
void label_table_init();

// save a label
void save_label(char *label, int eip);

// retrieve a label or return an error
int retrieve_label(char *name, int eip);

// deinitialize the label table
void label_table_deinit();

#endif
