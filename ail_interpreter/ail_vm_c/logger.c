#include "logger.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "line.h"

#define C_RED     "\x1b[31m"
#define C_RESET   "\x1b[0m"

char* get_time() {
    time_t timer;
    char *buffer = malloc(26);
    if (buffer == NULL) exit(-1);
    struct tm* tm_info;

    time(&timer);
    tm_info = localtime(&timer);

    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);

    return buffer;
}

void info(char *msg) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    printf(C_RED "%s %s\n" C_RESET, time, msg);
    free(time);
  }
}

void info_table(char *label, int eip) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: Saving label %s\n" C_RESET, time, l, label);
    free(l);
    free(time);
  }
}

void info_function(char *function, int eip) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: Saving function %s\n" C_RESET, time, function);
    free(l);
    free(time);
  }
}

void fatal(char *msg) {
  if (CURRENT_LOG_LEVEL <= FATAL) {
    char *time = get_time();
    printf(C_RED "%s %s\n" C_RESET, time, msg);
    free(time);
  }
}

void fatal_table(char *name, int eip) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: LabelNotFoundException (Label %s not found)\n" C_RESET, time, l, name);
    free(l);
    free(time);
  }
}

void fatal_function(char *name, int eip) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: FunctionNotFoundException (Function %s not found)\n" C_RESET, time, l, name);
    free(l);
    free(time);
  }
}

void fatal_variable(char *name, int eip) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: VariableNotFoundException (Var %s not found)\n" C_RESET, time, l, name);
    free(l);
    free(time);
  }
}

void info_variable(char *name, int eip) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: ScopeFallback (Var %s: not in scope. Falling back.\n" C_RESET, time, l, name);
    free(l);
    free(time);
  }
}

void info_variable_upscope(char *name, int eip, char *scope, char *new_scope) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: Upscoping (Var %s from %s to %s.\n" C_RESET, time, l, name, scope, new_scope);
    free(l);
    free(time);
  }
}

void info_variable_collapse(char* scope, int eip) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: Collapsing scope %s\n" C_RESET, time, l, scope);
    free(l);
    free(time);
  }
}

void info_variable_save(char *name, char *scope, int eip) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: Saving variable %s in scope %s\n" C_RESET, time, l, name, scope);
    free(l);
    free(time);
  }
}

void info_execution(char *instruction, int eip) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    char *l = line(eip);
    printf(C_RED "%s %s: Executing instruction %s" C_RESET, time, l, instruction);
    free(l);
    free(time);
  }
}

void info_code_array(int new_size) {
  if (CURRENT_LOG_LEVEL <= INFO) {
    char *time = get_time();
    printf(C_RED "%s: increased size of code array to %d\n" C_RESET, time, new_size);
    free(time);
  }
}


