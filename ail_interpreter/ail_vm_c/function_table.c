#include "function_table.h"
#include "map.h"
#include "line.h"
#include "logger.h"
#include <stdlib.h>
#include <stdio.h>

map_int_t function_table;

void function_table_init() {
  map_init(&function_table);
}

// save a function
void save_function(char *function, int eip) {
  int *address = map_get(&function_table, function);
  if (!address) {
    info_function(function, eip);
    map_set(&function_table, function, eip);
  } else {
    //there is nothing to do, since the function is already defined
    //gcc _should_ throw this branch away
  }
  
}

// retrieve a function or return an error
int retrieve_function(char *name, int eip) {
  int *address = map_get(&function_table, name);
  if (address) {
    return *address;
  } else {
    fatal_function(name, eip);
    exit(-1);
  }
}

void function_table_deinit() {
    map_deinit(&function_table);
}

