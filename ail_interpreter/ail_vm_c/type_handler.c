#include "type_handler.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char *simplify_num(char *d) {
  int len = strlen(d);
  char *ret = malloc(len);
  if (ret == NULL) exit(-1);
  if(len > 3 && !strcmp(d + len - 2, ".0")) {
    memcpy(ret, d, len - 2);
    return ret;
  } else {
    return d; 
  }
}


