#include <stdio.h>
#include <stdlib.h>
#include "executor.h"
#include "logger.h"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("Usage: %s [file.nail]\n", argv[0]);
    return(-1);
  }

  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;

  fp = fopen(argv[1], "r");
  if (fp == NULL) {
    return -1;
  }
  init();
  while ((read = getline(&line, &len, fp)) != -1) {
    scedule(line);
  }

  fclose(fp);
  if (line)
      free(line);

  while (execute_next()) {}
  deinit();
  return 0;
}
