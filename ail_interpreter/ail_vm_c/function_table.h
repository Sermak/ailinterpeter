#ifndef FUNCTION_TABLE_H_INCLUDED
#define FUNCTION_TABLE_H_INCLUDED

// initialize the function table
void function_table_init();

// save a function
void save_function(char *name, int eip);

// retrieve a function or return an error
int retrieve_function(char *name, int eip);

// deinitialize the function table
void function_table_deinit();

#endif
