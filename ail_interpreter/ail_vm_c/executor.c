#include "function_table.h"
#include "label_table.h"
#include "line.h"
#include "logger.h"
#include "get_token.h"
#include "type_handler.h"
#include "variable_table.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

char **code = NULL;
int free_position_in_code = 0;
int size_of_code = 0;
int eip = 0;

// initialize the executor
void init() {
  function_table_init();
  variable_table_init();
  label_table_init();
}

void increase_code_array() {
  size_of_code += 128;
  info_code_array(size_of_code);
  code = realloc(code, size_of_code * sizeof(char*));
  if (code == NULL) {
    printf("Out of memory\n"); //TODO: fatal log
    exit(-1);
  }
}

// schedule an instruction for later execution
void scedule(char *instruction) {
  char *tmp = t(instruction, 0);
  int opcode = atoi(tmp);
  free(tmp);
  if (opcode == 1) { // LABEL
    char *tmp = e(instruction, 0);
    save_label(tmp, free_position_in_code + 1);
    free(tmp - 1);

  } else if (opcode == 10) { // FUNCTION
    char *tmp = e(instruction, 0);
    save_function(tmp, free_position_in_code + 1);
    free(tmp - 1);
  }
  if (free_position_in_code == size_of_code) {
    increase_code_array();
  }
  char *inst = malloc(strlen(instruction) + 1);
  strcpy(inst, instruction);
  code[free_position_in_code] = inst;
  free_position_in_code++;
}

void execute(char* instruction) {
  char* tmp = t(instruction, 0);
  int opcode = atoi(tmp);
  free(tmp);
  printf("%d\n", opcode);
}

// execute the next instruction on the stack (It's actually an array)
bool execute_next() {
  if (free_position_in_code - 1 == eip) {
    return false;
  }
  char *instruction = code[eip];
  info_execution(instruction, eip);
  execute(instruction);
  eip += 1;
  return true;
}

// deinitialize the executor
void deinit() {
  for (int i = 0; i < free_position_in_code; i++) {
    free(code[i]);
  }
  free(code);
  function_table_deinit();
  variable_table_deinit();
  label_table_deinit();
}
