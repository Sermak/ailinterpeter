#include "variable_table.h"
#include "map.h"
#include "line.h"
#include "logger.h"
#include <stdlib.h>
#include <stdio.h>

struct variable {
  char *value;
  char *type;
};

typedef map_t(struct variable) map_variable_t;

map_variable_t variable_table;

void variable_table_init() {
  map_init(&variable_table);
}

//concat variable scope with name
char* concat(const char *scope, const char *name) {
    const size_t len1 = strlen(scope);
    const size_t len2 = strlen(name);
    char *result = malloc(len1 + len2 + 2); // +1 for the null-terminator, +1 for seperator
    if (!result) {exit(-1);}
    memcpy(result, scope, len1);
    memcpy(result + len1, SEPERATOR_STRING, 1); //TODO: or 2?
    memcpy(result + len1 + 1, name, len2 + 2); // +1 to copy the null-terminator, +1 for seperator
    return result;
}

char *get_scope(char *concatination) {
  char *scope = strtok(concatination, SEPERATOR_STRING);
  return scope;
}

// save a variable
void save_var(char *name, char *value, char *scope, int eip) {
  char *var = concat(scope, name);
  info_variable_save(name, scope, eip);
  struct variable to_save;
  to_save.value = value;
  // type not implemented?
  map_set(&variable_table, var, to_save);
}

int last_index_of(char *string, char c) {
  int j = 0;
  int curr_best = -1;
  while (string[j]) {
    if (string[j] == c && j > curr_best) {
      curr_best = j;
    }
    j++;
  }
  return curr_best;
}


/*   C equivalent of pythons
     s = scope
     s = s[:s.rindex("/")]
     s = s[:s.rindex("/")] + "/"
     return s
*/
char* scope_domain_down(char* scope){
  int l = last_index_of(scope, '/');
  char* tmp = malloc(strlen(scope));
  memcpy(tmp, scope, l);
  l = last_index_of(tmp, '/');
  char* tmp2 = malloc(strlen(scope));
  memcpy(tmp2, scope, l);
  tmp2 = strcat(tmp2, "/");
  free(tmp);
  return tmp2;
}

// retrieve a variable or return an error
char* retrieve_var(char *name, char *scope, int eip) {
  char *var = concat(scope, name);
  struct variable* ret = map_get(&variable_table, var);
  if (ret != NULL) {
    return ret->value;
  } else {
    if (strcmp(scope, "/") == 0) {
      fatal_variable(name, eip);
      return NULL;
    } else {
      char *tmp2 = scope_domain_down(scope);
      char* to_ret = retrieve_var(name, tmp2, eip);
      free(tmp2);
      info_variable(name, eip);
      return to_ret;
    }
  }
}

// kill all variables in the scope
void collapse_scope(char *scope, int eip) {
  info_variable_collapse(scope, eip);

  const char *key;
  map_iter_t iter = map_iter(&variable_table);
  while ((key = map_next(&variable_table, &iter))) {
    int len = strlen(key);
    char *copy_of_key = malloc(len); //TODO: null terminator?
    if (copy_of_key == NULL) {exit(-1);}
    strncpy(copy_of_key, key, len);
    if (strcmp(get_scope(copy_of_key), scope)) {
      map_remove(&variable_table, key);
    }
  }
}

// move a variable from a lower scope into a higher scope
void upscope_var(char *name, char *scope, int eip) {
  if (strcmp(scope, "/") == 0) {
    return;
  }
  char* s = scope_domain_down(scope);
  char* value_of_var = retrieve_var(name, scope, eip);
  info_variable_upscope(name, eip, scope, s);
  map_remove(&variable_table, name); // or first merge scope and name?
  save_var(name, value_of_var, s, eip);
  free(s);
}

// deinitialize the variable table
void variable_table_deinit() {
  map_deinit(&variable_table);
}
