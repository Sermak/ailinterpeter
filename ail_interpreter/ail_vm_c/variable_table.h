#ifndef VARIABLE_TABLE_H_INCLUDED
#define VARIABLE_TABLE_H_INCLUDED

#define SEPERATOR_STRING "|"

// initialize the variable table
void variable_table_init();

// save a variable
void save_var(char *name, char *value, char *scope, int eip);

// retrieve a variable or return an error
char* retrieve_var(char *name, char *scope, int eip);

// kill all variables in the scope
void collapse_scope(char *scope, int eip);

// move a variable from a lower scope into a higher scope
void upscope_var(char *name, char *scope, int eip);

// deinitialize the variable table
void variable_table_deinit();

#endif
