#ifndef GET_TOKEN_H_INCLUDED
#define GET_TOKEN_H_INCLUDED

// get token number n from string s
char* t(char* s, int n);

// get all tokens from string s except token n
char* e(char* s, int n);

// get the number of tokens
int n(char* s);

#endif
