#ifndef EXECUTOR_H_INCLUDED
#define EXECUTOR_H_INCLUDED
#include <stdbool.h>

// initialize the executor
void init();

// schedule an instruction for later execution
void scedule(char *instruction);

// execute the next instruction on the stack (It's actually an array)
bool execute_next();

// deinitialize the executor
void deinit();

#endif
