#include "label_table.h"
#include "map.h"
#include "line.h"
#include "logger.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

map_int_t label_table;

void label_table_init() {
  map_init(&label_table);
}

// save a label
void save_label(char *label, int eip) {
  int *address = map_get(&label_table, label);
  if (!address) {
    info_table(label, eip);
    map_set(&label_table, label, eip);
  } else {
    //there is nothing to do, since the label is already defined
    //gcc _should_ throw this branch away
  }
  
}

// retrieve a label or return an error
int retrieve_label(char *name, int eip) {
  int *address = map_get(&label_table, name);
  if (address) {
    return *address;
  } else {
    fatal_table(name, eip);
    exit(-1);
  }
}

void label_table_deinit() {
    map_deinit(&label_table);
}
