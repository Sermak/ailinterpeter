#include "line.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

char *line(int eip) {
  eip++;
  int n = eip;
  int len = 0;
  while(n != 0) {
      n /= 10;
      ++len;
  }
  char *ret = malloc(len + 1);
  if (ret == NULL) exit(-1);
  sprintf(ret, "%d", eip);
  return ret;
}

