#ifndef LOGGER_H_INCLUDED
#define LOGGER_H_INCLUDED

#define DEBUG 1
#define INFO 2
#define WARN 3
#define FATAL 4

#define CURRENT_LOG_LEVEL 1


//print info message if log level is high enough
void info(char *msg);

//print fatal message if log level is high enough
void fatal(char *msg);



//specific messages for the label_table
void info_table(char *label, int eip);
void fatal_table(char *name, int eip);

//specific messages for the function_table
void info_function(char *function, int eip);
void fatal_function(char *name, int eip);

//specific messages for the variable_table
void info_variable(char *name, int eip);
void fatal_variable(char *name, int eip);
void info_variable_upscope(char *name, int eip, char *scope, char *new_scope);
void info_variable_collapse(char *scope, int eip);
void info_variable_save(char *name, char *scope, int eip);

//specific messages for executor
void info_execution(char *instruction, int eip);
void info_code_array(int new_size);

#endif
