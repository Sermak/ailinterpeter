#include "get_token.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

//def t(s, n):
//    return s.split(" ")[n]
//
//def e(s, n):
//    a = ""
//    ss = s.split(" ")
//    for i in range(len(ss)):
//        if i != n:
//            a += ss[i] + " "
//    return a[:-1]
//
//

char* t(char* s, int n) {
  int num_spaces = 0;
  int len_of_token = 0;
  int token_start = -1;
  for (int i = 0; s[i]; i++) {
    if (s[i] == ' ') {
      num_spaces++;
    }
    if (num_spaces == n) {
      if (token_start == -1)
	token_start = i;
      len_of_token++;
    }
    if (token_start != -1 && num_spaces != n) {
      break;
    }
  }
  char *ret;
  if (n == 0)
    ret = calloc(sizeof(char), len_of_token + 2);
  else
    ret = calloc(sizeof(char), len_of_token + 1);
  if (ret == NULL) {
    printf("Out of memory\n"); //TODO: fatal log
    exit(-1);
  }
  if (n == 0) {
      memcpy(ret, &s[token_start], len_of_token);
      ret[len_of_token + 1] = '\0';
  } else {
    memcpy(ret, &s[token_start + 1], len_of_token - 1);
    ret[len_of_token] = '\0';
  }
  return ret;
}

char* e(char* s, int n) {
  int end = strlen(s);
  char *c = malloc(end);
  int i = 0;
  int j = 0;
  char number_of_spaces = 0;
  while (s[i]) {
    if (s[i] == ' ')
      ++number_of_spaces;
    if (number_of_spaces != n) 
      c[j++] = s[i];
    i++;
  }
  c[j] = '\0';
  return n == 0 ? c + 1 : c;
}

int n(char* s) {
  if (strlen(s) == 0) return 0;
  int i;
  for (i=0; s[i]; s[i]==' ' ? i++ : *s++);
  return i + 1;
}


