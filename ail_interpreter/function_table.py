from line import line
from logger import info, fatal
import sys


class FunctionTable:
    table = {}

    def save_function(self, name, eip):
        if name not in self.table:
            info(line(eip) + " - Saving function " + name + " at line " + line(eip))
            self.table[name] = eip

    def retrieve_function(self, name, eip):
        try:
            return self.table[name]
        except KeyError:
            fatal("FunctionNotFoundException (Line " + line(eip) + ": Function " + name + " not found)")
            sys.exit(-1)
