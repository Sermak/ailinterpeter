from line import line
from logger import info, fatal
import sys


class Lable_Table:
    table = {}

    def save_label(self, label, eip):
        if label not in self.table:
            info(line(eip) + " - Saving label " + label)
            self.table[label] = eip

    def retrieve_label(self, name, eip):
        try:
            return self.table[name]
        except KeyError:
            fatal("LabelNotFoundException (Line " + line(eip) + ": Label " + name + " not found)")
            sys.exit(-1)
