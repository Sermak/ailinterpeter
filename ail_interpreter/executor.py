import sys
import math
from ail_interpreter.function_table import FunctionTable  # done in c
from ail_interpreter.lable_table import Lable_Table  # done in c
from line import line  # done in c
from logger import info, fatal  # done in c
from get_token import t, e, n  # done in c
from ail_interpreter.type_handler import simplify_num  # done in c
from ail_interpreter.variable_table import VariableTable  # done in c


class Executor:
    code = []
    busy = False
    eip = 0
    lable_table = Lable_Table()
    function_table = FunctionTable()
    var_table = VariableTable()
    stack = []
    ret_stack = []
    addr_stack = []
    scope = "/"

    def schedule(self, instruction):
        opcode = int(t(instruction, 0))
        if opcode == 1:  # LABEL
            self.lable_table.save_label(e(instruction, 0), len(self.code))
        elif opcode == 10:  # FUNCTION
            self.function_table.save_function(e(instruction, 0), len(self.code))
        self.code.append(instruction)


    def execute_next(self):
        self.busy = True
        # at the end of code or illegal pointer
        if len(self.code) == self.eip or self.eip < 0:
            self.busy = False
            return False
        inst = self.code[self.eip]
        info(line(self.eip) + " - Executing instruction: " + inst)
        self.execute(inst)
        self.busy = False
        #print(self.eip)
        self.eip += 1
        return True

    def execute(self, instruction):
        opcode = int(t(instruction, 0))

        if opcode == 0:  # NOOP
            pass
        elif opcode == 1:  # LABEL
            pass  # LABELS are handled at schedule time
        elif opcode == 2:  # JUMP:
            self.eip = self.lable_table.retrieve_label(e(instruction, 0), self.eip)
        elif opcode == 3:  # PRINT
            print(e(instruction, 0))
        elif opcode == 4:  # PRINT_VAR
            print(self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip))
        elif opcode == 5:  # PUT
            self.var_table.save_var(t(instruction, n(instruction) - 1),
                                    e(e(instruction, 0), n(instruction) - 2), self.scope, self.eip)
        elif opcode == 6:  # MOVE
            old_var_value = self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip)
            self.var_table.save_var(t(instruction, 2), old_var_value, self.scope, self.eip)
        elif opcode == 7:  # ADD
            operand1_value = self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip)
            operand2_value = self.var_table.retrieve_var(t(instruction, 2), self.scope, self.eip)
            return_location = t(instruction, 3)
            try:
                op1 = float(operand1_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand1_value + " is not a number and cannot be added)")
                sys.exit(-1)
            try:
                op2 = float(operand2_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand2_value + " is not a number and cannot be added)")
                sys.exit(-1)
            result = op1 + op2
            self.var_table.save_var(return_location, simplify_num(result), self.scope, self.eip)
        elif opcode == 8:  # MULT
            operand1_value = self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip)
            operand2_value = self.var_table.retrieve_var(t(instruction, 2), self.scope, self.eip)
            return_location = t(instruction, 3)
            try:
                op1 = float(operand1_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand1_value + " is not a number and cannot be multiplied)")
                sys.exit(-1)
            try:
                op2 = float(operand2_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand2_value + " is not a number and cannot be multiplied)")
                sys.exit(-1)
            result = op1 * op2
            self.var_table.save_var(return_location, simplify_num(result), self.scope, self.eip)
        elif opcode == 9:  # CALL
            self.addr_stack.append(self.eip)
            self.eip = self.function_table.retrieve_function(e(instruction, 0), self.eip)
            self.scope += e(instruction, 0) + "/"
        elif opcode == 10:  # FUNCTION
            pass
        elif opcode == 11:  # RETURN
            self.eip = self.addr_stack.pop(len(self.addr_stack) - 1)
            self.var_table.collapse_scope(self.scope, self.eip)
            self.scope = self.scope[:self.scope.rindex("/")]
            self.scope = self.scope[:self.scope.rindex("/")] + "/"
        elif opcode == 12:  # POP
            self.var_table.save_var(e(instruction, 0), self.stack.pop(len(self.stack) - 1), self.scope, self.eip)
        elif opcode == 13:  # PUSH
            self.stack.append(e(instruction, 0))
        elif opcode == 14:  # PUSH_VAR
            self.stack.append(self.var_table.retrieve_var(e(instruction, 0), self.scope, self.eip))
        elif opcode == 15:  # RET_POP
            self.var_table.save_var(e(instruction, 0), self.ret_stack.pop(len(self.ret_stack) - 1), self.scope, self.eip)
        elif opcode == 16:  # RET_PUSH
            self.ret_stack.append(e(instruction, 0))
        elif opcode == 17:  # RET_PUSH_VAR
            self.ret_stack.append(self.var_table.retrieve_var(e(instruction, 0), self.scope, self.eip))
        elif opcode == 18:  # COMPARE
            operand1 = self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip)
            operand2 = self.var_table.retrieve_var(t(instruction, 2), self.scope, self.eip)
            return_location = t(instruction, 3)
            if operand1 == operand2:
                result = 0
            elif operand1 > operand2:
                result = -1
            elif operand1 < operand2:
                result = 1
            else:
                result = -10
            info(line(self.eip) + " - COMPARE " + operand1 + " " + operand2 + ": " + simplify_num(result))
            self.var_table.save_var(return_location, simplify_num(result), self.scope, self.eip)
        elif opcode == 19:  # IF
            variable = t(instruction, 1)
            labels = [x.strip() for x in e(e(instruction, 0), 0).split("ELSE")]
            if self.var_table.retrieve_var(variable, self.scope, self.eip) == "0":
                self.eip = self.lable_table.retrieve_label(labels[0], self.eip)
            else:
                self.eip = self.lable_table.retrieve_label(labels[1], self.eip)
        elif opcode == 20:  # SUB
            operand1_value = self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip)
            operand2_value = self.var_table.retrieve_var(t(instruction, 2), self.scope, self.eip)
            return_location = t(instruction, 3)
            info(line(self.eip) + " - Subtracting val of " + t(instruction, 1) + " - val of " + t(instruction, 2) + " into " + return_location)
            info(line(self.eip) + " - Subtracting " + operand1_value + " - " + operand2_value + " into " + return_location)
            try:
                op1 = float(operand1_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand1_value + " is not a number and cannot be subtracted)")
                sys.exit(-1)
            try:
                op2 = float(operand2_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand2_value + " is not a number and cannot be subtracted)")
                sys.exit(-1)
            result = op1 - op2
            self.var_table.save_var(return_location, simplify_num(result), self.scope, self.eip)
        elif opcode == 21:  # DIV
            operand1_value = self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip)
            operand2_value = self.var_table.retrieve_var(t(instruction, 2), self.scope, self.eip)
            return_location = t(instruction, 3)
            try:
                op1 = float(operand1_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand1_value + " is not a number and cannot be divided)")
                sys.exit(-1)
            try:
                op2 = float(operand2_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand2_value + " is not a number and cannot be divided)")
                sys.exit(-1)
            result = op1 / op2
            self.var_table.save_var(return_location, simplify_num(result), self.scope, self.eip)
        elif opcode == 22:  # MOD
            operand1_value = self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip)
            operand2_value = self.var_table.retrieve_var(t(instruction, 2), self.scope, self.eip)
            return_location = t(instruction, 3)
            try:
                op1 = float(operand1_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand1_value + " is not a number and cannot be divided)")
                sys.exit(-1)
            try:
                op2 = float(operand2_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand2_value + " is not a number and cannot be divided)")
                sys.exit(-1)
            result = divmod(op1, op2)
            self.var_table.save_var(return_location, simplify_num(result), self.scope, self.eip)
        elif opcode == 23:  # EXP
            operand1_value = self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip)
            operand2_value = self.var_table.retrieve_var(t(instruction, 2), self.scope, self.eip)
            return_location = t(instruction, 3)
            try:
                op1 = float(operand1_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand1_value + " is not a number and cannot be divided)")
                sys.exit(-1)
            try:
                op2 = float(operand2_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand2_value + " is not a number and cannot be divided)")
                sys.exit(-1)
            result = op1 ** op2
            self.var_table.save_var(return_location, simplify_num(result), self.scope, self.eip)
        elif opcode == 24:  # LOG
            operand1_value = self.var_table.retrieve_var(t(instruction, 1), self.scope, self.eip)
            operand2_value = self.var_table.retrieve_var(t(instruction, 2), self.scope, self.eip)
            return_location = t(instruction, 3)
            try:
                op1 = float(operand1_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand1_value + " is not a number and cannot be divided)")
                sys.exit(-1)
            try:
                op2 = float(operand2_value)
            except ValueError:
                fatal("IllegalCastError (Line " + line(self.eip) + ": " +
                      operand2_value + " is not a number and cannot be divided)")
                sys.exit(-1)
            result = math.log(op2, op1)
            self.var_table.save_var(return_location, simplify_num(result), self.scope, self.eip)
        elif opcode == 25:  # TAU
            self.var_table.save_var(e(instruction, 0), simplify_num(math.tau), self.scope, self.eip)
        elif opcode == 26:  # EULER
            self.var_table.save_var(e(instruction, 0), simplify_num(math.e), self.scope, self.eip)
        elif opcode == 27:  # UPSCOPE
            self.var_table.upscope_var(e(instruction, 0), self.scope, self.eip)
        elif opcode == 28:  # PROC_TIME
            import time
            self.var_table.save_var(e(instruction, 0), simplify_num(time.process_time()), self.scope, self.eip)
