from get_token import t, e
from ail_interpreter.ail_instruction_nums import ail_to_int


def normalize_to_num(from_path, to_path):
    with open(from_path) as f:
        content = [x.strip() for x in f.readlines()]
    for i in range(len(content)):
        content[i] = content[i].split("#")[0]
        content[i] = str(ail_to_int(t(content[i], 0), i)) + " " + e(content[i], 0)
    with open(to_path, 'w') as f:
        for i in content:
            f.write("%s\n" % i)
