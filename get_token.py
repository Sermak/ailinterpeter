# get token number n from s
def t(s, n):
    return s.split(" ")[n]


# get all tokens from s except token n
def e(s, n):
    a = ""
    ss = s.split(" ")
    for i in range(len(ss)):
        if i != n:
            a += ss[i] + " "
    return a[:-1]


# get the number of tokens
def n(s):
    return len(s.split(" "))