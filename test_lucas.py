import time

start_time = time.process_time()

def lucas(x):
    if x == 0:
        return 0
    elif x == 1:
        return 1
    else:
        return lucas(x-2) + lucas(x-1)


print(lucas(40))

end_time = time.process_time()

print(end_time - start_time)