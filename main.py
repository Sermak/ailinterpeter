import logging, os
from ail_interpreter.executor import Executor
from ail_interpreter.ail_normalize import normalize_to_num

# Paths
filename = "0010count_to_10"
from_path = os.path.join("ail_interpreter", "ail_src", filename + ".ail")
normalized_path = os.path.join("ail_interpreter", "ail_src", filename + ".nail")

# Normalize
normalize_to_num(from_path, normalized_path)
# Add to execute pipeline
with open(normalized_path) as f:
    source_code = [x.strip() for x in f.readlines()]

executor = Executor()

# Schedule everything
for x in source_code:
    executor.schedule(x)

# Execute everything
while executor.execute_next():
    pass
    #input()
