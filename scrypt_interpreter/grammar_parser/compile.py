import os
from get_token import t, e

grammar_folder = "scrypt_grammar"

grammar_files = [os.path.join(grammar_folder, f) for f in os.listdir(grammar_folder)]

grammars = []
compiled_prolog = "import scrypt_interpreter.grammar_parser.parsers as p\n\n"
compiled_prolog += "native_whitespace_parser = p.WhiteSpaceParser('native_whitespace_parser')\n" \
                   "native_string_parser = p.StringParser('native_string_parser')\n" \
                   "native_eof_parser = p.EofParser('native_eof_parser')\n" \
                   "\n"
compiled = ""


def is_symbol_statement(line):
    line = line.strip()[1:].strip()
    if (line.count("'") - line.count("\\'")) == 2:
        return line.startswith("'") and line.endswith("'")
    return False


def is_native_statement(grammar, line_num):
    line = grammar[line_num].strip()[1:].strip()
    if line.startswith("native_"):
        return True
    return False


def compile_native_statement(grammar, line_num):
    name = t(grammar[0], 0)
    return name + "_parser = " + grammar[line_num].strip()[1:].strip() + "_parser\n"


def compile_symbol_statement(name, line):
    line = line.strip()
    symbol = line.split("'")[1]
    return (name + "_parser = p.SymbolParser('" + name + "_parser', '" + symbol + "')") + "\n"


def compile_seq_statement(name, line, level):
    lev = "" if level == 0 else str(level)
    if is_symbol_statement(line):
        symbol = line.strip().split("'")[1]
        return (name + "_parser" + lev + " = p.SymbolParser('" + name + "_parser', '" + symbol + "')") + "\n"
    else:
        line = line.strip()[1:].strip().split(" ")
        parser_list = ""
        save_values = []
        for token in line:
            if ":" in token and token.split(":")[1][0] == "'" and token.split(":")[1][-1] == "'":
                continue  # TODO: string to be saved
            elif ":" in token:
                s_value = token.split(":")[0]
                token = token.split(":")[1]
                save_values += [s_value]
            else:
                save_values += [""]
            if token.startswith("(") and token.endswith(")"):
                parser_list += "p.MaybeParser(" + token[1:-1] + "_parser), "
            else:
                parser_list += token + "_parser, "
        if parser_list.endswith(", "):
            parser_list = parser_list[0:-2]
        return (name + "_parser" + lev + " = p.SequenceParser('" + name + "_parser" + lev +
                "', [" + parser_list + "], " + str(save_values) + ")") + "\n"


def compile_or_statement(grammar, level):
    name = t(grammar[0], 0)
    lev = "" if level == 0 else str(level)
    ret = ""
    list_parsers = ""
    for i in range(len(grammar[1:])):
        seq = grammar[1:][i].strip()
        ret += compile_seq_statement(name, seq, i + 1)
        list_parsers += name + "_parser" + str(i + 1) + ", "
    if list_parsers.endswith(", "):
        list_parsers = list_parsers[:-2]
    return ret + (name + "_parser" + lev + " = p.OrParser('" + name + "_parser" +
                  lev + "', [" + list_parsers + "])") + "\n"


for file in grammar_files:
    with open(file) as f:
        s = f.read()
        # filter out empty lines and implementation directives
        grammars.append([l for l in s.split("\n") if l.strip() is not "" and not l.strip().startswith(">")])

for grammar in grammars:
    if len(grammar) > 2:
        compiled += compile_or_statement(grammar, 0)
    elif is_symbol_statement(grammar[1]):
        compiled += compile_symbol_statement(t(grammar[0], 0), grammar[1])
    elif is_native_statement(grammar, 1):
        compiled += compile_native_statement(grammar, 1)
    else:
        compiled += (compile_seq_statement(t(grammar[0], 0), grammar[1], 0))


def get_list_already_defined(string):
    already_defined = ""
    for line in string.split("\n"):
        if not line.isspace():
            already_defined += line.split(" = ")[0].strip() + "\n"
    return already_defined + "\nnative_whitespace_parser\nnative_string_parser"


def resolve_dependencies_in_order(to_print, already_defined):
    result = ""
    still_to_print = ""
    for line in to_print.split("\n"):
        if "[" in line and "]" in line:
            dependencies = line.split("[")[1].split("]")[0]
            if not dependencies.isspace():
                all_deps_resolved = True
                for dep in dependencies.split(","):
                    all_deps_resolved = all_deps_resolved and dep.strip() in [x.strip()
                                                                              for x in already_defined.split("\n")]
                if all_deps_resolved:
                    result += line + "\n"
                else:
                    still_to_print += line + "\n"
        else:
            result += line + "\n"
    return result, still_to_print


r, stp = resolve_dependencies_in_order(compiled, get_list_already_defined(""))

compiled = compiled_prolog + r.strip()
while not r.isspace():
    r, stp = resolve_dependencies_in_order(stp, get_list_already_defined(compiled + r))
    compiled += "\n\n" + r.strip()


def resolve_circular_dependencies(stp):
    result = ""
    setters = ""
    for line in stp.split("\n"):
        if "[" in line and "]" in line:
            dependencies = line.split("[")[1].split("]")[0]
            result += line.replace(dependencies, "") + "\n"
            for dep in dependencies.split(","):
                setters += line.split("=")[0].strip() + ".append_parser(" + dep.strip() + ")" + "\n"
        else:
            result += line + "\n"
    return result + "\n" + setters


compiled += "\n" + resolve_circular_dependencies(stp)

with open("scrypt_parser.py", 'w') as result:
    result.write(compiled.strip() + "\n")
