from abc import ABC, abstractmethod
from logger import *
from scrypt_interpreter.datastructs.code import Code


class Parser(ABC):
    @abstractmethod
    def __init__(self, name):
        self.name = name
        pass

    @abstractmethod
    def parse(self, string):
        pass


class SymbolParser(Parser):
    def __init__(self, name, symbol):
        super().__init__(name)
        self.symbol = symbol

    def parse(self, string):
        if string.startswith(self.symbol):
            info(string + ": parsed " + self.symbol)
            c = Code()
            c.type = "Symbol"
            c.value = self.symbol
            return True, string[len(self.symbol):], c
        info("Expected " + self.symbol + ", got: " + string)
        return False, string, None


class WhiteSpaceParser(Parser):
    def __init__(self, name):
        super().__init__(name)

    def parse(self, string):
        c = Code()
        c.type = "Whitespace"
        if string.isspace():
            info("whitespace parsed")
            return True, "", c
        elif string == "" or not string[0].isspace():
            info("Expected whitespace, got: " + string)
            return False, string, None
        else:
            for i in range(1, len(string)):
                if not string[0:i].isspace():
                    info("whitespace parsed")
                    return True, string[(i - 1):], c
            return True, string[1:], c


class StringParser(Parser):

    def __init__(self, name):
        super().__init__(name)

    def parse(self, string):
        if string.startswith('"'):
            return False, string, None
        elif string == "":
            return False, string, None
            # TODO: THINK
        else:
            for i in reversed(range(len(string))):
                if '"' not in string[0:i]:
                    c = Code()
                    c.type = "String"
                    c.value = string[0:i]
                    return True, string[i:], c
            return False, string, None


class MaybeParser(Parser):
    def __init__(self, parser):
        super().__init__("Maybe " + parser.name)
        self.parser = parser

    def parse(self, string):
        success, rest, code = self.parser.parse(string)
        if success:
            info(string + ": parsed " + self.parser.name)
            return True, rest, code
        else:
            return True, string, code


class EofParser(Parser):
    def __init__(self, name):
        super().__init__(name)

    def parse(self, string):
        if string is "":
            info("Parsed [eof]")
            return True, "", None
        else:
            info("Expected [eof], got: " + string)
            return False, string, None


class SequenceParser(Parser):
    def __init__(self, name, parsers, save_values):
        super().__init__(name)
        self.save_values = save_values
        self.parsers = parsers

    def parse(self, string):
        rest = string
        c = None
        d = None
        seqcode = Code()
        seqcode.type = self.name
        for i in range(len(self.parsers)):
            parser = self.parsers[i]
            success, rest, code = parser.parse(rest)
            if c is None:
                c = code
            elif d is None:
                d = code
                """
                c.next = d
            else:
                d.next = code
                d = code"""
            if not success:
                return False, string, None
            if self.save_values[i] is not "":
                value_len = len([x for x in self.save_values if x != ""])
                """if value_len == 1:
                    seqcode = code
                else:"""
                seqcode.__setattr__(self.save_values[i], code)

        info(string + ": parsed " + self.name)

        return True, rest, seqcode

    def append_parser(self, parser):
        self.parsers.append(parser)


class OrParser(Parser):
    def __init__(self, name, parsers):
        super().__init__(name)
        self.parsers = parsers

    def parse(self, string):
        for parser in self.parsers:
            success, rest, code = parser.parse(string)
            if success:
                info(string + ": parsed " + parser.name)
                return True, rest, code
            # info(string + ": parsing as " + self.name + " failed: expected: " + parser.name)
        return False, string, None

    def append_parser(self, parser):
        self.parsers.append(parser)
