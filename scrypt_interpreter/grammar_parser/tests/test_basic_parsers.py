import scrypt_interpreter.grammar_parser.parsers as p


def test_symbol_parser():
    symbol = "hello world"
    string = "hello world! What a nice day"
    symbol_parser = p.SymbolParser(symbol)
    assert (True, "! What a nice day") == symbol_parser.parse(string)


def test_maybe_parser():
    symbol = "hello world"
    string = "hello world! What a nice day"
    string2 = "good morning! What a nice day"
    symbol_parser = p.SymbolParser(symbol)
    maybe_symbol_parser = p.MaybeParser(symbol_parser)
    assert (True, "! What a nice day") == maybe_symbol_parser.parse(string)
    assert (True, string2) == maybe_symbol_parser.parse(string2)


def test_sequence_parser():
    symbol1 = "hello"
    symbol2 = " "
    symbol3 = "world"
    string = "hello world!"
    symbol1_parser = p.SymbolParser(symbol1)
    symbol2_parser = p.SymbolParser(symbol2)
    symbol3_parser = p.SymbolParser(symbol3)
    seq_parser = p.SequenceParser([symbol1_parser, symbol2_parser, symbol3_parser])
    assert (True, "!") == seq_parser.parse(string)


def test_or_parser():
    symbol1 = "a"
    symbol2 = "b"
    string_no_1 = ""
    string_no_2 = "c"
    string_yes_1 = "a"
    string_yes_2 = "ba"
    string_yes_3 = "aca"
    symbol1_parser = p.SymbolParser(symbol1)
    symbol2_parser = p.SymbolParser(symbol2)
    or_parser = p.OrParser([symbol1_parser, symbol2_parser])
    assert (False, string_no_1) == or_parser.parse(string_no_1)
    assert (False, string_no_2) == or_parser.parse(string_no_2)
    assert (True, "") == or_parser.parse(string_yes_1)
    assert (True, "a") == or_parser.parse(string_yes_2)
    assert (True, "ca") == or_parser.parse(string_yes_3)


def test_parsers():
    toParse1 = "aaaa"
    toParse2 = "aaba"
    toParse3 = "aabc"
    dontParse = "aabba"
    grammar = "aa(b)a | aa(b)c"

    a_parser = p.SymbolParser('a')
    b_parser = p.SymbolParser('b')
    c_parser = p.SymbolParser('c')
    maybe_b_parser = p.MaybeParser(b_parser)
    sequence1_parser = p.SequenceParser([a_parser, a_parser, maybe_b_parser, a_parser])
    sequence2_parser = p.SequenceParser([a_parser, a_parser, maybe_b_parser, c_parser])
    or_parser = p.OrParser([sequence1_parser, sequence2_parser])

    assert (True, "a") == or_parser.parse(toParse1)
    assert (True, "") == or_parser.parse(toParse2)
    assert (True, "") == or_parser.parse(toParse3)
    assert (False, dontParse) == or_parser.parse(dontParse)
