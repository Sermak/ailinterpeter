import os
from ail_interpreter.executor import Executor
from ail_interpreter.ail_normalize import normalize_to_num
import scrypt_interpreter.grammar_parser.scrypt_parser as p
from scrypt_interpreter.parsetree2ail.scry_assembler import expression2ail
from scrypt_interpreter.parsetree2ail.toail import toail

filename = "0001simpleprint"
filepath = os.path.join("..", "scrypt_src", filename + ".scrypt")
compilepath = os.path.join("..", "scry_compiled", filename)


with open(filepath) as f:
    src_code = f.read()


success, rest, code = p.expression_parser.parse(src_code)

if not success:
    print("Compilation failed")
    exit()


with open(compilepath + ".ail", "w+") as f:
    f.write(expression2ail(code)[0])

# Normalize
normalize_to_num(compilepath + ".ail", compilepath + ".nail")
# Add to execute pipeline
with open(compilepath + ".nail") as f:
    source_code = [x.strip() for x in f.readlines()]

executor = Executor()

# Schedule everything
for x in source_code:
    executor.schedule(x)

# Execute everything
while executor.execute_next():
    pass
    #input()
