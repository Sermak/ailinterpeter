from logger import *
from scrypt_interpreter.parsetree2ail.assemblers import get_unique_name


def boolexp2ail(code):
    final_name = get_unique_name("bool")
    ret = "PUT "
    if code.type == "boolean_parser1":
        ret += "1 "
        return ret + final_name, final_name
    else:
        fatal("Unsupported boolean exp")


def while2ail(code):
    if code.type == "while_parser":
        # compute boolean into variable beforehand
        ret = ""
        preamble = boolexp2ail(code.condition)
        ret += preamble[0] + "\n"

        # start of while loop
        start_label = get_unique_name("while")
        body_label = get_unique_name("while_body")
        end_label = get_unique_name("while_end")
        ret += "LABEL " + start_label + "\n"

        # boolean comparision
        ret += "IF " + preamble[1] + " " + end_label + " ELSE " + body_label + "\n"
        ret += "LABEL " + body_label + "\n"
        ret += block2ail(code.block)
        ret += "JUMP " + start_label + "\n"
        ret += "LABEL " + end_label + "\n"

        return ret
    else:
        fatal("Unsupported while exp")


def print2ail(code):
    if code.type == "print_parser1":
        ret = "PRINT " + str(code.argument.value.value)
        if code.tag.value == "pl":
            return ret + "\n"
    else:
        fatal("Unsupported print exp")


def expression2ail(code):
    if code.type == "single_expression_parser2":
        return while2ail(code.__getattribute__("while"))
    elif code.type == "single_expression_parser3":
        return print2ail(code.__getattribute__("print"))
    else:
        fatal("Unsupported expression: " + str(code))
    return str(code)


def block2ail(code):
    if code.type == "block_parser1":
        return expression2ail(code.__getattribute__("se"))
    else:
        fatal("Unsupported expression: " + str(code))
    return str(code)


def toail(code):
    if code.type == "expression_parser1":
        return expression2ail(code.se)
    return str(code)
