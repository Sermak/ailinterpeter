import os
from logger import info
from scrypt_interpreter.parsetree2ail.assemblers import get_unique_name

grammar_folder = "../grammar_parser/scrypt_grammar"
assembler_path = "./scry_assembler.py"

grammar_files = [os.path.join(grammar_folder, f) for f in os.listdir(grammar_folder)]

grammars = []
compiled_prolog = "from logger import *\n"
compiled_prolog += "from scrypt_interpreter.parsetree2ail.assemblers import get_unique_name\n\n"

compiled = compiled_prolog

# indent
I: str = "    "

# read and filter grammar files
for file in grammar_files:
    with open(file) as f:
        s = f.read()
        # filter out empty lines
        grammars.append([l for l in s.split("\n") if l.strip() is not ""])


def process_directive(id, token_dict):
    id = id.strip()[2:]
    ret = ""
    for word in id.split(" "):
        ret += token_dict.get(word, "\"" + word + "\"") + " + \" \" + "

    if ret.strip().endswith("+"):
        ret = ret.strip()[:-1]
    return 2 * I + "ret += " + ret.strip() + " + \"\\n\"\n"


def preprocess_directives(directives, names):
    precalculations = 2 * I + "ret = \"\"\n"
    precalculations += 2 * I + "final_name = \"undefined\"\n"
    token_dict = {}

    if directives.strip() is not "":
        for directive in directives.split("\n"):
            if directive.strip() is not "":
                for word in directive.strip()[2:].split(" "):
                    if word.startswith("»") and word.endswith("«"):
                        if word not in token_dict:
                            if "ſ" + word[1:-1] not in token_dict:
                                dataname = word[1:-1] + "_data"
                                if word[1:-1] in names:
                                    precalculations += 2 * I + dataname + " = " \
                                                       + names[word[1:-1]] \
                                                       + "2ail(code.__getattribute__(\"" + word[1:-1] + "\"))\n"
                                else:
                                    info("This here does not exist: " + word)
                            varname = word[1:-1] + "_code"
                            precalculations += 2 * I + varname + " = " + dataname + "[0]\n"
                            token_dict[word] = varname
                    elif word.startswith("§") and word not in token_dict:
                        varname = word[2:] if word[1:].startswith("§") else word[1:]
                        varname += "_label"
                        token_dict[word] = varname
                        precalculations += 2 * I + varname + " = get_unique_name(\"" + (word[2:] if word[1:].startswith("§") else word[1:]) + "\")\n"
                        if word.startswith("§§"):
                            precalculations += 2 * I + "final_name = " + varname + "\n"
                    elif word.startswith("$") and word not in token_dict:
                        token_dict[word] = "str(code." + word[1:] + ")"
                    elif word.startswith("ſ") and word not in token_dict:
                        if "»" + word[1:] + "«" not in token_dict:
                            dataname = word[1:] + "_data"
                            if word[1:] in names:
                                precalculations += 2 * I + dataname + " = " \
                                                   + names[word[1:]] \
                                                   + "2ail(code.__getattribute__(\"" + word[1:] + "\"))\n"
                            else:
                                info("This here does not exist: " + word)
                        varname = word[1:] + "_ret"
                        precalculations += 2 * I + varname + " = " + dataname + "[1]\n"
                        token_dict[word] = varname
    return precalculations, token_dict


def make_dictionary_from_parsing_directive(parsing_directive):
    dictionary = {}
    for word in parsing_directive.split(" "):
        if ":" in word:
            dictionary[word.split(":")[0]] = word.split(":")[1]
    return dictionary


def process_directives(implementation_directives, parsing_directive):
    names = make_dictionary_from_parsing_directive(parsing_directive)
    assembly_code, token_dict = preprocess_directives(implementation_directives, names)
    ret = assembly_code
    number_of_directives = 0

    for id in implementation_directives.split("\n"):
        if id.strip() is not "":
            ret += process_directive(id, token_dict)
            number_of_directives += 1

    if number_of_directives == 0:
        return 2 * I + "fatal(\"Unsupported expression: \" + str(code))\n"

    return ret + 2*I + "return ret, final_name" + "\n"


def make_function_body(grammar, name):
    options = ""
    implementation_directives = ""
    last_grammar = ""
    grammar_index = 1

    for i in range(1, len(grammar)):
        if grammar[i].strip().startswith("|"):
            if i is not 1:
                options += process_directives(implementation_directives, last_grammar.strip()[2:])
            implementation_directives = ""
            number = str(grammar_index)
            if grammar_index == 1:
                number = "¶ŧĥ€¶ŧĥ€ðđĝĥĵ"
            elif grammar_index == 2:
                options = options.replace("¶ŧĥ€¶ŧĥ€ðđĝĥĵ", "1")
            options += I + "if code.type == \"" + name + "_parser" + number + "\":\n"
            grammar_index += 1
            options += 2 * I + "# " + str(grammar[i]).strip()[2:] + "\n"
            last_grammar = str(grammar[i])
        else:
            implementation_directives += str(grammar[i]) + "\n"
    options = options.replace("¶ŧĥ€¶ŧĥ€ðđĝĥĵ", "")
    options += process_directives(implementation_directives, str(last_grammar).strip()[2:])
    return options


for g in grammars:
    name = g[0].split(" ")[0]
    compiled += ("\ndef " + name + "2ail(code):") + "\n"
    compiled += make_function_body(g, name)
    compiled += I + "else:\n"
    compiled += 2 * I + "fatal(\"Unsupported parser type: \" + str(code))\n\n"

import pprint

pprint.pprint(grammars)

with open(assembler_path, "w") as f:
    f.write(compiled)
