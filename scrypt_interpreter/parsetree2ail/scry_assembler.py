from logger import *
from scrypt_interpreter.parsetree2ail.assemblers import get_unique_name


def string2ail(code):
    if code.type == "string_parser":
        # string_delimiter value:native_string string_delimiter
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def expression_separator2ail(code):
    if code.type == "expression_separator_parser":
        # ';'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def expression2ail(code):
    if code.type == "expression_parser1":
        # se:single_expression (w) eof
        ret = ""
        final_name = "undefined"
        se_data = single_expression2ail(code.__getattribute__("se"))
        se_code = se_data[0]
        ret += se_code + " " + "\n"
        return ret, final_name
    if code.type == "expression_parser2":
        # se1:single_expression newline se2:single_expression (w) eof
        ret = ""
        final_name = "undefined"
        se1_data = single_expression2ail(code.__getattribute__("se1"))
        se1_code = se1_data[0]
        se2_data = single_expression2ail(code.__getattribute__("se2"))
        se2_code = se2_data[0]
        ret += se1_code + " " + "\n"
        ret += se2_code + " " + "\n"
        return ret, final_name
    if code.type == "expression_parser3":
        # se1:single_expression (w) expression_separator (w) se2:single_expression (w) eof
        ret = ""
        final_name = "undefined"
        se1_data = single_expression2ail(code.__getattribute__("se1"))
        se1_code = se1_data[0]
        se2_data = single_expression2ail(code.__getattribute__("se2"))
        se2_code = se2_data[0]
        ret += se1_code + " " + "\n"
        ret += se2_code + " " + "\n"
        return ret, final_name
    else:
        fatal("Unsupported parser type: " + str(code))


def scope_open2ail(code):
    if code.type == "scope_open_parser":
        # '{'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def bool_false2ail(code):
    if code.type == "bool_false_parser1":
        # 'false'
        fatal("Unsupported expression: " + str(code))
    if code.type == "bool_false_parser2":
        # 'False'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def bracket_open2ail(code):
    if code.type == "bracket_open_parser":
        # '('
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def eof2ail(code):
    if code.type == "eof_parser":
        # native_eof
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def print2ail(code):
    if code.type == "print_parser1":
        # tag:print_line_tag w argument:string
        ret = ""
        final_name = "undefined"
        ret += "PRINT" + " " + str(code.argument.value.value) + " " + "\n"
        return ret, final_name
    if code.type == "print_parser2":
        # tag:print_tag w argument:string
        fatal("Unsupported expression: " + str(code))
    if code.type == "print_parser3":
        # tag:print_line_tag bracket_open (w) argument:string (w) bracket_close
        fatal("Unsupported expression: " + str(code))
    if code.type == "print_parser4":
        # tag:print_tag bracket_open (w) argument:string (w) bracket_close
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def scope_close2ail(code):
    if code.type == "scope_close_parser":
        # '}'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def newline2ail(code):
    if code.type == "newline_parser":
        # '\n'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def string_delimiter2ail(code):
    if code.type == "string_delimiter_parser":
        # '"'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def block2ail(code):
    if code.type == "block_parser1":
        # se:single_expression
        ret = ""
        final_name = "undefined"
        se_data = single_expression2ail(code.__getattribute__("se"))
        se_code = se_data[0]
        ret += se_code + " " + "\n"
        return ret, final_name
    if code.type == "block_parser2":
        # se1:single_expression newline se2:single_expression
        fatal("Unsupported expression: " + str(code))
    if code.type == "block_parser3":
        # se1:single_expression (w) expression_separator (w) se2:single_expression
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def while2ail(code):
    if code.type == "while_parser":
        # while_tag w condition:boolean (w) scope_open (w) block:block (w) (newline) (w) scope_close
        ret = ""
        final_name = "undefined"
        condition_data = boolean2ail(code.__getattribute__("condition"))
        condition_code = condition_data[0]
        while_label = get_unique_name("while")
        condition_ret = condition_data[1]
        while_end_label = get_unique_name("while_end")
        while_body_label = get_unique_name("while_body")
        block_data = block2ail(code.__getattribute__("block"))
        block_code = block_data[0]
        ret += condition_code + " " + "\n"
        ret += "LABEL" + " " + while_label + " " + "\n"
        ret += "IF" + " " + condition_ret + " " + while_end_label + " " + "ELSE" + " " + while_body_label + " " + "\n"
        ret += "LABEL" + " " + while_body_label + " " + "\n"
        ret += block_code + " " + "\n"
        ret += "JUMP" + " " + while_label + " " + "\n"
        ret += "LABEL" + " " + while_end_label + " " + "\n"
        return ret, final_name
    else:
        fatal("Unsupported parser type: " + str(code))


def w2ail(code):
    if code.type == "w_parser":
        # native_whitespace
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def print_line_tag2ail(code):
    if code.type == "print_line_tag_parser":
        # 'pl'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def single_expression2ail(code):
    if code.type == "single_expression_parser1":
        # bracket_open (w) se:single_expression (w) bracket_close
        fatal("Unsupported expression: " + str(code))
    if code.type == "single_expression_parser2":
        # while:while
        ret = ""
        final_name = "undefined"
        while_data = while2ail(code.__getattribute__("while"))
        while_code = while_data[0]
        ret += while_code + " " + "\n"
        return ret, final_name
    if code.type == "single_expression_parser3":
        # print:print
        ret = ""
        final_name = "undefined"
        print_data = print2ail(code.__getattribute__("print"))
        print_code = print_data[0]
        ret += print_code + " " + "\n"
        return ret, final_name
    if code.type == "single_expression_parser4":
        # boolean:boolean
        fatal("Unsupported expression: " + str(code))
    if code.type == "single_expression_parser5":
        # string:string
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def while_tag2ail(code):
    if code.type == "while_tag_parser":
        # 'while'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def bool_true2ail(code):
    if code.type == "bool_true_parser1":
        # 'true'
        fatal("Unsupported expression: " + str(code))
    if code.type == "bool_true_parser2":
        # 'True'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def bracket_close2ail(code):
    if code.type == "bracket_close_parser":
        # ')'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def boolean2ail(code):
    if code.type == "boolean_parser1":
        # value:bool_true
        ret = ""
        final_name = "undefined"
        bool_label = get_unique_name("bool")
        final_name = bool_label
        ret += "PUT" + " " + "1" + " " + bool_label + " " + "\n"
        return ret, final_name
    if code.type == "boolean_parser2":
        # value:bool_false
        fatal("Unsupported expression: " + str(code))
    if code.type == "boolean_parser3":
        # bracket_open (w) value:boolean (w) bracket_close
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def print_tag2ail(code):
    if code.type == "print_tag_parser":
        # 'p'
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))


def empty_line2ail(code):
    if code.type == "empty_line_parser":
        # newline newline
        fatal("Unsupported expression: " + str(code))
    else:
        fatal("Unsupported parser type: " + str(code))

