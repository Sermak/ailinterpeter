class Code:
    pass

    def __eq__(self, other):
        return self.__str__() == other.__str__()

    def __str__(self):
        s = ""
        for i in self.__dir__():
            if not (i.startswith("__") and i.endswith("__")):
                if isinstance(i, Code):
                    s += "(" + str(s) + "), "
                else:
                    if isinstance(self.__getattribute__(i), Code):
                        s += str(i) + ": " + "(" + str(self.__getattribute__(i)) + "), "
                    else:
                        s += str(i) + ": " + str(self.__getattribute__(i)) + ", "

        if s.endswith(", "):
            s = s[:-2]

        return s
