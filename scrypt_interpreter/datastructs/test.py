from scrypt_interpreter.datastructs.code import Code

code = Code()
code.type = "while"
code.block = Code()
code.block.type = "pl"
code.block.argument = Code()
code.block.argument.value = "hello"

print(code)